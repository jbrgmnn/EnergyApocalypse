package eu.jacktrain.energyapocalypse.database;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.bukkit.Location;
import org.bukkit.World;

import com.avaje.ebean.validation.NotNull;

import eu.jacktrain.energyapocalypse.game.Game;

@Entity()
@Table(name = "ea_villager")
public class BeanVillager {
	
	@Id
	private int Id;
	@NotNull
	private String game;
	@NotNull
	private int Xloc;
	@NotNull
	private int YLoc;
	@NotNull
	private int ZLoc;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public int getXloc() {
		return Xloc;
	}
	public void setXloc(int xloc) {
		Xloc = xloc;
	}
	public int getZLoc() {
		return ZLoc;
	}
	public void setZLoc(int zLoc) {
		ZLoc = zLoc;
	}
	public int getYLoc() {
		return YLoc;
	}
	public void setYLoc(int yLoc) {
		YLoc = yLoc;
	}
	
	public void saveLoc(Location l,Game g){
		setXloc((int) l.getX());
		setYLoc((int) l.getY());
		setZLoc((int) l.getZ());
		setGame(g.getName());
	}
	public Location loadLoc(World w){
		return new Location(w,getXloc(),getYLoc(),getZLoc());
	}
	public String getGame() {
		return game;
	}
	public void setGame(String game) {
		this.game = game;
	}
	
}
