package eu.jacktrain.energyapocalypse.database;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.bukkit.Location;

import com.avaje.ebean.validation.NotNull;

import eu.jacktrain.energyapocalypse.game.Game;
import eu.jacktrain.energyapocalypse.game.team.EnergySource;
import eu.jacktrain.energyapocalypse.game.team.Team;

@Entity()
@Table(name = "ea_energysource")
public class BeanEnergySource {
	
	@Id
	private int Id;
	@NotNull
	private String game;
	@NotNull
	private String team;
	@NotNull
	private int Xloc;
	@NotNull
	private int YLoc;
	@NotNull
	private int ZLoc;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getGame() {
		return game;
	}
	public void setGame(String game) {
		this.game = game;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public int getXloc() {
		return Xloc;
	}
	public void setXloc(int xloc) {
		Xloc = xloc;
	}
	public int getYLoc() {
		return YLoc;
	}
	public void setYLoc(int yLoc) {
		YLoc = yLoc;
	}
	public int getZLoc() {
		return ZLoc;
	}
	public void setZLoc(int zLoc) {
		ZLoc = zLoc;
	}
	
	public void saveEnergySource(EnergySource e){
		Location l = e.getLocation();
		setXloc((int) l.getX());
		setYLoc((int) l.getY());
		setZLoc((int) l.getZ());
		setTeam(e.getTeam().getName());
		setGame(e.getG().getName());
	}
	public EnergySource loadEnergySource(Game g,Team t){
		Location l = new Location(g.getBounds().getWorld(),getXloc(),getYLoc(),getZLoc());
		return new EnergySource(l,t,g);
	}

}
