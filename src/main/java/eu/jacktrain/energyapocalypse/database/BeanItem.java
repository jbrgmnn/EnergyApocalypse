package eu.jacktrain.energyapocalypse.database;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

import com.avaje.ebean.validation.NotNull;

import eu.jacktrain.energyapocalypse.game.Game;
import eu.jacktrain.energyapocalypse.game.item.ItemDropper;

@Entity()
@Table(name = "ea_item")
public class BeanItem {
	
	@Id
	private int Id;
	@NotNull
	private String game;
	@NotNull
	private String type;
	@NotNull
	private int xLoc;
	@NotNull
	private int yLoc;
	@NotNull
	private int zLoc;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getGame() {
		return game;
	}
	public void setGame(String name) {
		this.game = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getXLoc() {
		return xLoc;
	}
	public void setXLoc(int d) {
		this.xLoc = d;
	}
	public int getYLoc() {
		return yLoc;
	}
	public void setYLoc(int d) {
		this.yLoc = d;
	}
	
	public int getZLoc() {
		return zLoc;
	}
	public void setZLoc(int zLoc) {
		this.zLoc = zLoc;
	}
	public void saveItemDropper(ItemDropper i,Game g){
		setGame(g.getName());
		setType(i.getItemStack().getType().name());
		setXLoc((int) i.getLocation().getX());
		setYLoc((int) i.getLocation().getY());
		setZLoc((int) i.getLocation().getZ());
	}
	public ItemDropper loadItemDropper(World gameWorld){
		Location l = new Location(gameWorld,xLoc,yLoc,zLoc);
		return new ItemDropper(l,Material.matchMaterial(type));
		
	}
}
