package eu.jacktrain.energyapocalypse.database;

import com.avaje.ebean.validation.NotNull;

import eu.jacktrain.energyapocalypse.game.Game;
import eu.jacktrain.energyapocalypse.game.team.Team;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;

@Entity()
@Table(name = "ea_team")
public class BeanTeam {

	@Id
	private int Id;
	@NotNull
	private String name;
	@NotNull
	private String game;
	@NotNull
	private String chatcolor;
	@NotNull
	private int xLoc;
	@NotNull
	private int yLoc;
	@NotNull
	private int zLoc;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getChatcolor() {
		return chatcolor;
	}
	public void setChatcolor(String chatcolor) {
		this.chatcolor = chatcolor;
	}
	public int getXLoc() {
		return xLoc;
	}
	public void setXLoc(int xLoc) {
		this.xLoc = xLoc;
	}
	public int getYLoc() {
		return yLoc;
	}
	public void setYLoc(int yLoc) {
		this.yLoc = yLoc;
	}
	public int getZLoc() {
		return zLoc;
	}
	public void setZLoc(int zLoc) {
		this.zLoc = zLoc;
	}
	public String getGame() {
		return game;
	}
	public void setGame(String game) {
		this.game = game;
	}
	public void saveTeam(Team team,Game g){
		setGame(g.getName());
		setName(team.getName());
		setChatcolor(""+team.getC().getChar());
		setXLoc((int) team.getSpawn().getX());
		setYLoc((int) team.getSpawn().getY());
		setZLoc((int) team.getSpawn().getZ());
	}
	public Team loadTeam(Game g,World w){
		Location s = new Location(w,getXLoc(),getYLoc(),getZLoc());
		return new Team(s,getName(),ChatColor.getByChar(getChatcolor()),g);
	}
	
}
