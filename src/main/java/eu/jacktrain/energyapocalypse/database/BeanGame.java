package eu.jacktrain.energyapocalypse.database;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.bukkit.Bukkit;
import org.bukkit.World;

import com.avaje.ebean.validation.NotNull;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.game.Game;

@Entity()
@Table(name = "ea_game")
public class BeanGame {
	
	@Id
	private int Id;
	@NotNull
	private String name;
	@NotNull
	private int x1;
	@NotNull
	private int z1;
	@NotNull
	private int x2;
	@NotNull
	private int z2;
	@NotNull
	private String worldUUID;
	
	public String getName() {
		return name;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getX1() {
		return x1;
	}
	public void setX1(int x1) {
		this.x1 = x1;
	}
	public int getZ1() {
		return z1;
	}
	public void setZ1(int z1) {
		this.z1 = z1;
	}
	public int getX2() {
		return x2;
	}
	public void setX2(int x2) {
		this.x2 = x2;
	}
	public int getZ2() {
		return z2;
	}
	public void setZ2(int z2) {
		this.z2 = z2;
	}
	public String getWorldUUID() {
		return worldUUID;
	}
	public void setWorldUUID(String worldUUID) {
		this.worldUUID = worldUUID;
	}
	
	public void saveGame(Game g){
		setName(g.getName());
		setX1(g.getBounds().getBounds()[0]);
		setZ1(g.getBounds().getBounds()[1]);
		setX2(g.getBounds().getBounds()[2]);
		setZ2(g.getBounds().getBounds()[3]);
		setWorldUUID(g.getBounds().getWorld().getUID().toString());
	}
	public Game loadGame(EnergyApocalypse core){
		World world = Bukkit.getWorld(UUID.fromString(getWorldUUID()));
		return new Game(getName(),getX1(),getZ1(),getX2(),getZ2(),world,core);
	}
}
