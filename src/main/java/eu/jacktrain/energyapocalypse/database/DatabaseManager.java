package eu.jacktrain.energyapocalypse.database;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;

import com.avaje.ebean.EbeanServer;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.game.Game;
import eu.jacktrain.energyapocalypse.game.GameBuild;
import eu.jacktrain.energyapocalypse.game.GameChat;
import eu.jacktrain.energyapocalypse.game.GameCombat;
import eu.jacktrain.energyapocalypse.game.GameComponent;
import eu.jacktrain.energyapocalypse.game.GameRespawn;
import eu.jacktrain.energyapocalypse.game.GameVillager;
import eu.jacktrain.energyapocalypse.game.GameWorld;
import eu.jacktrain.energyapocalypse.game.item.GameResources;
import eu.jacktrain.energyapocalypse.game.item.ItemDropper;
import eu.jacktrain.energyapocalypse.game.team.Team;

public class DatabaseManager {
	private EnergyApocalypse core;
	private EbeanServer db;
	
	public DatabaseManager(EnergyApocalypse ncore) {
		core = ncore;
		db = core.getDatabase();
	}
	
	public void save(Game game){
		BeanGame bg  = new BeanGame();
		bg.saveGame(game);
		db.save(bg);
		for(Team t : game.getTeams()){
			BeanEnergySource be = new BeanEnergySource();
			be.saveEnergySource(t.getEnergySource());
			db.save(be);
			BeanTeam bt = new BeanTeam();
			bt.saveTeam(t,game);
			db.save(bt);
		}
		GameResources gr = game.getGameResources();
		for(ItemDropper i : gr.getDropper1()){
			BeanItem bi = new BeanItem();
			bi.saveItemDropper(i,game);
			db.save(bi);
		}
		for(ItemDropper i : gr.getDropper2()){
			BeanItem bi = new BeanItem();
			bi.saveItemDropper(i,game);
			db.save(bi);
		}
		for(ItemDropper i : gr.getDropper3()){
			BeanItem bi = new BeanItem();
			bi.saveItemDropper(i,game);
			db.save(bi);
		}
		GameVillager gv = game.getGameVillager();
		for(Location l : gv.getSpawnpoints()){
			BeanVillager bv = new BeanVillager();
			bv.saveLoc(l,game);
			db.save(bv);
		}
	}
	
	public void loadAll(){
		Set<BeanGame> set = db.find(BeanGame.class).findSet();
		for(BeanGame bg : set){
			Game g = bg.loadGame(core);
			load(g);
		}
	}

	public void load(Game g) {
		HashSet<Team> teams = new HashSet<Team>();
		Set<BeanTeam> setT = db.find(BeanTeam.class).where().eq("game",g.getName()).findSet();
		for(BeanTeam t : setT){
			Team tm = t.loadTeam(g, g.getBounds().getWorld());
			tm.setGame(g);
			BeanEnergySource es = db.find(BeanEnergySource.class).where().eq("game",g.getName()).eq("team", tm.getName()).findUnique();
			tm.setEnergySource(es.loadEnergySource(g, tm));
			teams.add(tm);
		}
		HashSet<ItemDropper> items = new HashSet<ItemDropper>();
		Set<BeanItem> setI = db.find(BeanItem.class).where().eq("game",g.getName()).findSet();
		for(BeanItem i : setI){
			items.add(i.loadItemDropper(g.getBounds().getWorld()));
		}
		HashSet<Location> villager = new HashSet<Location>();
		Set<BeanVillager> setV = db.find(BeanVillager.class).where().eq("game",g.getName()).findSet();
		for(BeanVillager bv : setV){
			villager.add(bv.loadLoc(g.getBounds().getWorld()));
		}
		
		HashSet<GameComponent> components = new HashSet<GameComponent>();
		components.add(new GameResources(items,core));
		components.add(new GameVillager(g,villager));
		components.add(new GameChat(g));
		components.add(new GameRespawn(g));
		components.add(new GameCombat(g));
		components.add(new GameBuild(g));
		components.add(new GameWorld(g));
		
		g.setTeams(teams);
		g.setComponents(components);
		core.addGame(g);
	}
	
	public void delete(Game g){
		String n = g.getName();
		BeanGame bg = db.find(BeanGame.class).where().eq("name", n).findUnique();
		db.delete(bg);
		Set<BeanTeam> setT = db.find(BeanTeam.class).where().eq("game",g.getName()).findSet();
		db.delete(setT);
		Set<BeanItem> setI = db.find(BeanItem.class).where().eq("game",g.getName()).findSet();
		db.delete(setI);
		Set<BeanVillager> setV = db.find(BeanVillager.class).where().eq("game",g.getName()).findSet();
		db.delete(setV);
		Set<BeanEnergySource> setE = db.find(BeanEnergySource.class).where().eq("game",g.getName()).findSet();
		db.delete(setE);
		core.removeGame(g);
	}
}
