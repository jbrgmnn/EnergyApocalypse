package eu.jacktrain.energyapocalypse.game;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.game.team.Team;

public class GameCombat extends GameComponent implements Listener {
	private Game game;
	private EnergyApocalypse core;

	public GameCombat(Game g) {

		game = g;
		core = game.getCore();

		core.getServer().getPluginManager().registerEvents(this, core);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void deathCheckEntity(EntityDamageByEntityEvent event) {
		if (event.getEntityType().equals(EntityType.PLAYER)
				&& event.getDamager().getType().equals(EntityType.PLAYER)) {
			Player ply = (Player) event.getEntity();
			Player dam = (Player) event.getDamager();
			for (Team temp : game.getTeams()) {
				if (temp.isMember(ply) && temp.isMember(dam)) {
					event.setCancelled(true);
				}
			}
		}
	}

	public void delete() {
		EntityDamageByEntityEvent.getHandlerList().unregister(this);
	}
}
