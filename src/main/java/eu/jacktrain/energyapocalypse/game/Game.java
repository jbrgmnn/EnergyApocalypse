package eu.jacktrain.energyapocalypse.game;

import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.core.Presets;
import eu.jacktrain.energyapocalypse.game.item.GameResources;
import eu.jacktrain.energyapocalypse.game.shop.GameTrade;
import eu.jacktrain.energyapocalypse.game.team.SpectatorManager;
import eu.jacktrain.energyapocalypse.game.team.Team;

public class Game implements Listener {
	private HashSet<GameComponent> components;
	private HashSet<Team> teams;
	private GameTrade trade;
	private GameBounds bounds;
	private PreGameManager pregame;
	private String name;
	private EnergyApocalypse core;
	private String status;
	private SpectatorManager spectators;
	private Objective objective;
	private Scoreboard scoreboard;

	public Game(String nname, int x1, int z1, int x2, int z2, World nworld,
			EnergyApocalypse ncore) {

		name = nname;
		core = ncore;
		trade = new GameTrade(this);
		bounds = new GameBounds(this, nworld, x1, z1, x2, z2);
		pregame = new PreGameManager(this, core);
		status = "Pregame";
		
		teams = new HashSet<Team>();
		components = new HashSet<GameComponent>();
		spectators = new SpectatorManager(null,core,this);
		core.getServer().getPluginManager().registerEvents(this, core);

		scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		objective = scoreboard.registerNewObjective("Energy", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName("Energy");
	}

	public boolean cmd(String[] args, CommandSender sender) {
		if (args[0].equalsIgnoreCase(name)) {
			if (args.length > 1) {
				if (args[1].equalsIgnoreCase("join")) {
					if (checkJoinPerm(sender)) {
						if (sender instanceof Player) {
							Player ply = (Player) sender;
							if (status.equals("Pregame")) {
								if (core.isInGame(ply)||pregame.isMember(ply)) {
									sender.sendMessage(Presets.PREFIX
											+ "You can't join twice");
								} else {
									pregame.join(ply);
								}
							} else {
								sender.sendMessage(Presets.PREFIX
										+ "You can't join a " + status
										+ " game !");
							}
						} else {
							sender.sendMessage(Presets.NEED_PLAYER);
						}
						return true;
					}
				} else if (args[1].equalsIgnoreCase("leave")) {
					if (checkLeavePerm(sender)) {
						if (sender instanceof Player) {
							Player ply = (Player) sender;
								if(pregame.isMember(ply)){
									pregame.leave(ply);
								}
								else if(spectators.isMember(ply)){
									spectators.removeMember(ply);
								}
								else{
									sender.sendMessage(Presets.PREFIX+"You can't leave while in game");
								}

						} else {
							sender.sendMessage(Presets.NEED_PLAYER);
						}
						return true;	
					}
				} else if (args[1].equalsIgnoreCase("spectate")) {
					if (sender.hasPermission(Presets.PERM_ADMIN+"spectate")) {
						if (sender instanceof Player) {
							Player ply = (Player) sender;
							if(!core.isInGame(ply)){
								spectators.addMember(ply);
							}
							else{
								sender.sendMessage(Presets.PREFIX
										+ "You can't join twice");
							}

						} else {
							sender.sendMessage(Presets.NEED_PLAYER);
						}
						return true;
					}

				} else if (args[1].equalsIgnoreCase("start")) {
					if (sender.hasPermission(Presets.PERM_ADMIN + "start")) {
						if (status.equals("Pregame")) {
							start();
							if (sender instanceof Player) {
								boolean b = false;
								Player ply = (Player) sender;
								for (Team temp : teams) {
									if (temp.isMember(ply)) {
										b = true;
									}
								}
								if (!b) {
									sender.sendMessage(Presets.PREFIX + "Game "
											+ name + " started!");
								}
							} else {
								sender.sendMessage(Presets.PREFIX + "Game "
										+ name + " started!");
							}
						} else {
							sender.sendMessage(Presets.PREFIX
									+ "Game already started!");
						}
						return true;

					}
				} else if (args[1].equalsIgnoreCase("stop")) {
					if (sender.hasPermission(Presets.PERM_ADMIN + "stop")) {		
						stop();
						return true;

					}
				}
			} else {
				if (sender.hasPermission(Presets.PERM_ADMIN + "game")) {
					sender.sendMessage(Presets.PREFIX + "Game " + name
							+ " status: " + status);
					return true;
				}
			}
		}

		return false;

	}

	private boolean checkJoinPerm(CommandSender sender) {
		if(core.isUsePlayPerm()){
			return sender.hasPermission(Presets.PERM_PLAYER + "join");
		}
		return true;
	}
	private boolean checkLeavePerm(CommandSender sender) {
		if(core.isUsePlayPerm()){
			return sender.hasPermission(Presets.PERM_PLAYER + "leave");
		}
		return true;
	}
	private void stop() {
		Team winner = null;
		for (Team t : teams) {
			if (winner != null) {
				if (winner.getEnergySource().getHealth() < t.getEnergySource()
						.getHealth()) {
					winner = t;
				}
			} else {
				winner = t;
			}
		}
		Bukkit.broadcastMessage(Presets.PREFIX + "Game " + name
				+ " was stopped");
		win(winner);

	}

	private void start() {
		pregame.start();
		for (Team temp : teams) {
			temp.start();
		}
		for (GameComponent temp : components) {
			temp.start();
			;
		}
		bc(Presets.PREFIX + "Game started!");
		status = "running";

	}

	private void win(Team temp) {
		temp.win();
		Bukkit.broadcastMessage(Presets.PREFIX + ChatColor.RED + "Team "
				+ temp.getColorName() + ChatColor.RED + " has won " + name
				+ " !");
		status = "finished";
		deleteSubSystems();
	}

	private void deleteSubSystems() {
		for (GameComponent g : components) {
			g.delete();
		}
		for (Team t : teams) {
			t.clear();
		}
		
		components.clear();
		trade.delete();
		bounds.delete();
		trade.delete();
		spectators.clear();
	}

	public boolean isInGame(Player ply) {
		boolean b = false;
		for (Team temp : teams) {
			if (temp.isMember(ply)) {
				b = true;
			}
		}
		if (spectators.isMember(ply)) {
			b = true;
		}
		return b;
	}

	public void bc(String s) {
		for (Team temp : teams) {
			temp.bc(s);
		}
		spectators.bc(s);
	}

	public void playSound(Sound s) {
		for (Team temp : teams) {
			temp.playSound(s);
			;
		}
		spectators.playSound(s);
	}
	public void delete(){
		deleteSubSystems();
	}
	
	public void checkWinner() {
		int ingame = 0;
		for(Team t : teams){
			if(t.getMemberCount()>0){
				ingame++;
			}
		}
		if(ingame==1){
			for(Team t : teams){
				if(t.getMemberCount()>0){
					win(t);
				}
			}
		}
		else if (ingame<1){
			status = "finished";
			deleteSubSystems();
		}
		
	}

	/*
	 * set/get area
	 */

	public String getName() {
		return name;
	}

	public SpectatorManager getSpectators() {
		return spectators;
	}

	public void setSpectators(SpectatorManager spectators) {
		this.spectators = spectators;
	}

	public String getStatus() {
		return status;
	}

	public Objective getObjective() {
		return objective;
	}

	public Scoreboard getScoreboard() {
		return scoreboard;
	}

	public GameTrade getGameTrade() {
		return trade;
	}

	public GameBounds getBounds() {
		return bounds;
	}

	public PreGameManager getPregame() {
		return pregame;
	}

	public EnergyApocalypse getCore() {
		return core;
	}

	public void setComponents(HashSet<GameComponent> components) {
		this.components = components;
	}

	public void setTeams(HashSet<Team> teams) {
		this.teams = teams;
		spectators.clear();
		spectators = new SpectatorManager(((Team) teams.toArray()[0])
				.getSpawn().clone().add(0, 50, 0), core, this);
	}

	public HashSet<Team> getTeams() {
		return teams;
	}

	public GameResources getGameResources() {
		for (GameComponent g : components) {
			if (g instanceof GameResources) {
				return (GameResources) g;
			}
		}
		return null;
	}

	public GameVillager getGameVillager() {
		for (GameComponent g : components) {
			if (g instanceof GameVillager) {
				return (GameVillager) g;
			}
		}
		return null;
	}



}