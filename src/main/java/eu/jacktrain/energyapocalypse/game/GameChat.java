package eu.jacktrain.energyapocalypse.game;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.core.Presets;
import eu.jacktrain.energyapocalypse.game.team.Team;

@SuppressWarnings("deprecation")
public class GameChat extends GameComponent implements Listener {

	private Game game;
	private EnergyApocalypse core;

	public GameChat(Game g) {

		game = g;
		core = game.getCore();

		core.getServer().getPluginManager().registerEvents(this, core);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void chatCheck(PlayerChatEvent event) {
		core = game.getCore();
		for (Team temp : game.getTeams()) {
			if (temp.isMember(event.getPlayer())) {
				if (event.getMessage().startsWith("@a")) {
					Bukkit.broadcastMessage(Presets.PREFIX + "[SHOUT]"
							+ event.getPlayer().getDisplayName() + ":"
							+ event.getMessage().substring(2));
					event.setCancelled(true);
				} else {
					event.setCancelled(true);
					String msg = Presets.PREFIX + "[" + temp.getColorName()
							+ "]" + event.getPlayer().getDisplayName() + ": "
							+ event.getMessage();
					core.getLogger().info(ChatColor.stripColor(msg));
					temp.bc(msg);
				}
			}
		}
	}

	public void delete() {
		PlayerChatEvent.getHandlerList().unregister(this);
	}
}
