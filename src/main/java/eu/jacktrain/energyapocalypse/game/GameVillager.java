package eu.jacktrain.energyapocalypse.game;

import java.util.HashSet;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;

public class GameVillager extends GameComponent implements Listener {

	private GameBounds bounds;
	private HashSet<Location> spawnpoints;
	private EnergyApocalypse core;

	public GameVillager(Game game, HashSet<Location> nspawnpoints) {

		core = game.getCore();
		spawnpoints = nspawnpoints;
		bounds = game.getBounds();

		core.getServer().getPluginManager().registerEvents(this, core);
	}

	public HashSet<Location> getSpawnpoints() {
		return spawnpoints;
	}

	public void start() {
		core.getServer().getScheduler()
				.scheduleSyncDelayedTask(core, new Runnable() {
					public void run() {
						for (Location l : spawnpoints) {
							Villager v = (Villager) l.getWorld().spawnEntity(l.add(new Vector(0.5 ,0.5, 0.5)),
									EntityType.VILLAGER);
							v.setProfession(Profession.PRIEST);
							v.setCustomName("Equipment");
							v.setCustomNameVisible(true);
							v.setNoDamageTicks(Integer.MAX_VALUE);
						}
					}
				}, 20L);

	}

	@EventHandler(priority = EventPriority.HIGH)
	public void damageCheck(EntityDamageEvent event) {
		if (event.getEntity() instanceof Villager) {
			if (!bounds.isOutside(event.getEntity().getLocation())) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void damegeCheckEntity(EntityDamageByEntityEvent event) {
		if (event.getEntity() instanceof Villager) {
			if (!bounds.isOutside(event.getEntity().getLocation())) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void damegeCheckEntity(EntityDamageByBlockEvent event) {
		if (event.getEntity() instanceof Villager) {
			if (!bounds.isOutside(event.getEntity().getLocation())) {
				event.setCancelled(true);
			}
		}
	}
	public void delete(){
		EntityDamageByBlockEvent.getHandlerList().unregister(this);
		EntityDamageByEntityEvent.getHandlerList().unregister(this);
		EntityDamageEvent.getHandlerList().unregister(this);
	}
}
