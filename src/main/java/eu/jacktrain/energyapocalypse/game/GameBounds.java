package eu.jacktrain.energyapocalypse.game;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class GameBounds extends GameComponent implements Listener {
	private Game game;
	private int[] bounds;
	private World world;

	public GameBounds(Game g, World w, int x1, int z1, int x2, int z2) {
		world = w;
		game = g;
		bounds = new int[4];
		if (x1 > x2) {
			bounds[0] = x2;
			bounds[2] = x1;
		} else {
			bounds[0] = x1;
			bounds[2] = x2;
		}
		if (z1 > z2) {
			bounds[1] = z2;
			bounds[3] = z1;
		} else {
			bounds[1] = z1;
			bounds[3] = z2;
		}
		game.getCore().getServer().getPluginManager()
				.registerEvents(this, game.getCore());
	}

	public int[] getBounds() {
		return bounds;
	}

	public void setBounds(int[] bounds) {
		this.bounds = bounds;
	}

	public void setWorld(World world) {
		this.world = world;
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void moveCheck(PlayerMoveEvent event) {
		if (game.isInGame(event.getPlayer())
				&& (!game.getPregame().isMember(event.getPlayer()))) {
			event.setCancelled(isOutside(event.getTo()));
		}
	}

	public boolean isOutside(Location to) {
		boolean w = to.getWorld().equals(world);
		boolean x1 = (to.getBlockX() > bounds[0]);
		boolean x2 = (bounds[2] > to.getBlockX());
		boolean z1 = (bounds[1] < to.getBlockZ());
		boolean z2 = (to.getBlockZ() < bounds[3]);
		return !(w && x1 && x2 && z1 && z2);
	}

	public void delete() {
		PlayerMoveEvent.getHandlerList().unregister(this);
	}

	public World getWorld() {
		return world;
	}

	public boolean isOutside(Chunk chunk) {
		boolean a = isOutside(new Location(chunk.getWorld(),chunk.getX()*16,0,chunk.getZ()*16));
		boolean b = isOutside(new Location(chunk.getWorld(),chunk.getX()*16+16,0,chunk.getZ()*16+16));
		return (a||b);
	}

}
