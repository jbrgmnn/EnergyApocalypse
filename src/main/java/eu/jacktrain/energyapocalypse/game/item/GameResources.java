package eu.jacktrain.energyapocalypse.game.item;

import java.util.HashSet;

import org.bukkit.Material;
import org.bukkit.scheduler.BukkitTask;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.game.GameComponent;

public class GameResources extends GameComponent implements Runnable {

	private HashSet<ItemDropper> dropper1;
	private HashSet<ItemDropper> dropper2;
	private HashSet<ItemDropper> dropper3;

	private BukkitTask task;
	private int counter;
	private EnergyApocalypse core;

	public GameResources(HashSet<ItemDropper> ndropper,EnergyApocalypse ncore) {
		dropper1 = new HashSet<ItemDropper>();
		dropper2 = new HashSet<ItemDropper>();
		dropper3 = new HashSet<ItemDropper>();
		for(ItemDropper d: ndropper){
			if(d.getItemStack().getType().equals(Material.CLAY_BRICK)){
				dropper1.add(d);
			}
			else if(d.getItemStack().getType().equals(Material.IRON_INGOT)){
				dropper2.add(d);
			}
			else if(d.getItemStack().getType().equals(Material.GOLD_INGOT)){
				dropper3.add(d);
			}
		}
		core = ncore;
	}

	public HashSet<ItemDropper> getDropper1() {
		return dropper1;
	}

	public HashSet<ItemDropper> getDropper2() {
		return dropper2;
	}

	public HashSet<ItemDropper> getDropper3() {
		return dropper3;
	}

	public void start() {
		task = core.getServer().getScheduler().runTaskTimer(core, this, 1, 20);
	}

	public void delete() {
		task.cancel();
	}

	public void run() {
		counter++;
		for (ItemDropper d : dropper1) {
			d.drop();
		}
		if (counter == 10 || counter == 20) {
			for (ItemDropper d : dropper2) {
				d.drop();
			}
		} else if (counter == 30) {
			for (ItemDropper d : dropper2) {
				d.drop();
			}
			for (ItemDropper d : dropper3) {
				d.drop();
			}
			counter = 0;
		}

	}
}
