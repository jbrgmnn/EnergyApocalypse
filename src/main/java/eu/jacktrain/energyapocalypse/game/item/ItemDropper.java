package eu.jacktrain.energyapocalypse.game.item;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemDropper {

	private Location location;
	private ItemStack item;

	public ItemDropper(Location loc, Material m) {
		location = loc;
		item = new ItemStack(m,1);
	}

	public void drop() {
		location.getWorld().dropItem(location, item);
	}

	public Location getLocation(){
		return location;
	}
	public ItemStack getItemStack(){
		return item;
	}
}
