package eu.jacktrain.energyapocalypse.game.team;

import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.core.Presets;
import eu.jacktrain.energyapocalypse.game.Game;

@SuppressWarnings("deprecation")
public class SpectatorManager implements Listener {

	private HashSet<Player> members;
	private HashSet<UUID> disconected;
	private HashMap<Player, String> defNames;
	private static ChatColor c = ChatColor.GRAY;
	private Location spawn;
	private EnergyApocalypse core;
	private Game g;

	public SpectatorManager(Location nspawn, EnergyApocalypse ncore, Game ng) {
		members = new HashSet<Player>();
		disconected = new HashSet<UUID>();
		defNames = new HashMap<Player, String>();
		core = ncore;
		g = ng;
		spawn = nspawn;
		core.getServer().getPluginManager().registerEvents(this, core);
	}

	public void addMember(final Player ply) {
		defNames.put(ply, ply.getDisplayName());
		ply.setDisplayName(c + ply.getName() + ChatColor.WHITE);
		ply.setPlayerListName(c + ply.getName());
		ply.getInventory().clear();
		ply.getInventory().setBoots(null);
		ply.getInventory().setLeggings(null);
		ply.getInventory().setChestplate(null);
		ply.getInventory().setHelmet(null);
		ply.setHealth(ply.getMaxHealth());
		ply.setFoodLevel(20);
		ply.setGameMode(GameMode.SPECTATOR);
		ply.setScoreboard(g.getScoreboard());
		ply.playSound(ply.getLocation(), Sound.ANVIL_USE, 1, 1);
		ply.sendMessage(Presets.PREFIX + "You are a spectator now!");
		ply.sendMessage(Presets.PREFIX + "Type: '/a " + g.getName()
				+ " leave' to leave the game");
		members.add(ply);
		core.getServer().getScheduler()
				.scheduleSyncDelayedTask(core, new Runnable() {
					public void run() {
						ply.teleport(spawn);
					}
				}, 1L);

	}

	public void addOffline(Player ply) {
		disconected.add(ply.getUniqueId());
	}

	public void removeMember(Player ply) {
		removePlayer(ply);
		members.remove(ply);

	}

	private void removePlayer(final Player ply) {
		ply.sendMessage(Presets.PREFIX + "You left the game");
		core.getServer().getScheduler()
				.scheduleSyncDelayedTask(core, new Runnable() {
					public void run() {
						ply.teleport(ply.getLocation().getWorld()
								.getSpawnLocation());
					}
				}, 2L);
		ply.setDisplayName(defNames.get(ply));
		ply.setPlayerListName(ply.getName());
		ply.getInventory().clear();
		ply.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		ply.setGameMode(Bukkit.getDefaultGameMode());

	}

	public void clear() {
		for (Player temp : members) {
			removePlayer(temp);
		}
		defNames.clear();
		members.clear();
		disconected.clear();
		PlayerChatEvent.getHandlerList().unregister(this);
		PlayerQuitEvent.getHandlerList().unregister(this);
		PlayerJoinEvent.getHandlerList().unregister(this);
	}

	public void bc(String s) {
		for (Player temp : members) {
			temp.sendMessage(s);
		}
	}

	public void playSound(Sound s) {
		for (Player temp : members) {
			temp.playSound(temp.getLocation(), s, 1, 1);
		}
	}

	public boolean isMember(Player ply) {
		return members.contains(ply);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void chatCheck(PlayerChatEvent event) {
		if (isMember(event.getPlayer())) {
			if (event.getMessage().startsWith("@a")) {
				Bukkit.broadcastMessage(Presets.PREFIX + "[SHOUT]"
						+ event.getPlayer().getDisplayName() + ":"
						+ event.getMessage().substring(2));
				event.setCancelled(true);
			} else {
				event.setCancelled(true);
				String msg = Presets.PREFIX + "[" + ChatColor.GRAY
						+ "Spectator" + ChatColor.WHITE + "]"
						+ event.getPlayer().getDisplayName() + ": "
						+ event.getMessage();
				core.getLogger().info(ChatColor.stripColor(msg));
				bc(msg);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void quitCheck(PlayerQuitEvent event) {
		Player ply = event.getPlayer();
		if (g.isInGame(ply) && (ply.getGameMode() == GameMode.SURVIVAL)) {
			for (Team t : g.getTeams()) {
				if (t.isMember(ply)) {
					t.removeMember(ply);
					g.bc(Presets.PREFIX
							+ "Player "
							+ ply.getDisplayName()
							+ " was removed from his team, because he disconnected");
					addOffline(ply);
					g.checkWinner();
				}
			}

		} else if (members.contains(event.getPlayer())) {
			disconected.add(ply.getUniqueId());
			removeMember(ply);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void joinCheck(PlayerJoinEvent event) {
		Player ply = event.getPlayer();
		if (disconected.contains(ply.getUniqueId())) {
			addMember(ply);
			disconected.remove(ply.getUniqueId());
		}
	}

	public void leave(Player ply) {
		if (isMember(ply)) {
			removeMember(ply);
		} else {
			ply.sendMessage(Presets.PREFIX
					+ " You can't leave a running game !");
		}

	}

}
