package eu.jacktrain.energyapocalypse.game.team;

import java.util.Collection;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.LargeFireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.Score;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.game.Game;

public class EnergySource implements Runnable {
	private int health;
	private Location loc;
	private BukkitTask task;
	private Team team;
	private Random random;
	private Game g;
	private EnergyApocalypse core;
	private int silence;
	private Score score;
	private int explosion;

	public EnergySource(Location nloc, Team t, Game ng) {
		g = ng;
		health = 1000;
		core = g.getCore();
		team = t;
		random = new Random();
		loc = nloc;
		score = ng.getObjective().getScore(team.getColorName());
		score.setScore(health);
	}
	public Team getTeam() {
		return team;
	}

	public Game getG() {
		return g;
	}

	public void run() {
		Collection<? extends Player> p = Bukkit.getOnlinePlayers();
		for (Player temp : p) {
			if (((temp.getGameMode() == GameMode.SURVIVAL) && (g.isInGame(temp) && (!team
					.isMember(temp)))) && temp.getLocation().distance(loc) < 2) {
				damage(2);

				if (silence > 20) {
					loc.getWorld().playEffect(loc, Effect.MOBSPAWNER_FLAMES,
							null);
					temp.playSound(temp.getLocation(), Sound.CLICK, 1, 1);
					team.playSound(Sound.ANVIL_LAND);
					silence = 0;
				}
			}
		}
		if (explosion > 0) {
			explosion++;
			if(explosion>10){
				loc.getWorld().strikeLightning(loc);
			}
			if (explosion > 15) {
				 LargeFireball l1 = loc.getWorld().spawn(loc, LargeFireball.class);
				 l1.setYield(0);
				 LargeFireball l2 = loc.getWorld().spawn(loc, LargeFireball.class);
				 l2.setYield(0);
				
				loc.getWorld().strikeLightning(loc);
				task.cancel();
			}
		}
		silence++;
		if (random.nextInt(100) > 50) {
			loc.getWorld().playEffect(loc, Effect.SMOKE, getDir());
		}
	}

	public void start() {
		task = core.getServer().getScheduler().runTaskTimer(core, this, 1, 1);
	}

	public void die() {
		team.loseSpawn();
		explosion++;
	}

	public void damage(int i) {
		if (health > i) {
			health = health - i;
		} else {
			health = 0;
		}
		score.setScore(health);
		if (!(health > 0) && (explosion < 1)) {
			die();
		}
	}

	public int getHealth() {
		return health;
	}

	public Location getLocation() {
		return loc;
	}

	public int getDir() {
		return random.nextInt(9);
	}

}
