package eu.jacktrain.energyapocalypse.game.team;

import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import eu.jacktrain.energyapocalypse.core.Presets;
import eu.jacktrain.energyapocalypse.game.Game;

public class Team {

	private HashSet<Player> members;
	private HashMap<Player,String> defNames;
	private String name;
	private EnergySource energySource;
	private Location spawn;
	private ChatColor c;
	private Game game;
	private boolean respawning;

	public Team(Location nspawn, String nname, ChatColor nc, Game ngame) {
		spawn = nspawn;
		name = nname;
		game = ngame;
		c = nc;
		members = new HashSet<Player>();
		defNames = new HashMap<Player,String>();
		respawning = true;
	}

	public String getName() {
		return name;
	}

	public ChatColor getC() {
		return c;
	}

	public void addMember(Player ply) {
		defNames.put(ply, ply.getDisplayName());
		if (c != null) {
			ply.setDisplayName(c + ply.getName() + ChatColor.WHITE);
			ply.setPlayerListName(c + ply.getName());
		}
		ply.setExp(0);
		ply.getInventory().clear();
		ply.getInventory().setBoots(null);
		ply.getInventory().setLeggings(null);
		ply.getInventory().setChestplate(null);
		ply.getInventory().setHelmet(null);
		ply.setHealth(ply.getMaxHealth());
		ply.setFoodLevel(20);
		ply.teleport(spawn);
		ply.setScoreboard(game.getScoreboard());
		ply.setGameMode(GameMode.SURVIVAL);
		ply.playSound(ply.getLocation(), Sound.ANVIL_USE, 1, 1);
		ply.sendMessage(Presets.PREFIX + "You are in team " + c + name);
		members.add(ply);
	}

	public void removeMember(Player ply) {
		ply.setDisplayName(defNames.get(ply));
		ply.setPlayerListName(ply.getName());
		ply.getInventory().clear();
		ply.getInventory().setBoots(null);
		ply.getInventory().setLeggings(null);
		ply.getInventory().setChestplate(null);
		ply.getInventory().setHelmet(null);
		ply.setGameMode(Bukkit.getDefaultGameMode());
		ply.teleport(ply.getLocation().getWorld().getSpawnLocation());
		ply.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		members.remove(ply);
		if (members.isEmpty()) {
			bc(Presets.PREFIX + "Team " + getColorName() + " left the area");
			game.checkWinner();
			clear();
		}
	}

	public boolean start() {
		energySource.start();
		return (energySource != null) && (game != null);
	}

	public int getMemberCount(){
		return members.size();
	}
	
	public void clear() {
		defNames.clear();
		for (Player temp : members) {
			removeMember(temp);
		}
	}

	public void bc(String s) {
		for (Player temp : members) {
			temp.sendMessage(s);
		}
	}

	public void playSound(Sound s) {
		for (Player temp : members) {
			temp.playSound(temp.getLocation(), s, 1, 1);
			;
		}
	}

	public EnergySource getEnergySource() {
		return energySource;
	}

	public String getColorName() {
		return c + name + ChatColor.WHITE;
	}

	public boolean respawning() {
		return respawning;
	}

	public boolean isMember(Player ply) {
		return members.contains(ply);
	}

	public void setEnergySource(EnergySource energySource) {
		this.energySource = energySource;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Location getSpawn() {
		return spawn;
	}

	public void loseSpawn() {
		respawning = false;
		game.playSound(Sound.ENDERDRAGON_DEATH);
		game.bc(Presets.PREFIX + "The power source of team " + c + name
				+ ChatColor.WHITE + " is empty");

	}

	public void win() {
		playSound(Sound.ENDERMAN_DEATH);
	}

}
