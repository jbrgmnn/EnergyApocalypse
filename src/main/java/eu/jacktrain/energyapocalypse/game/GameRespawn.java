package eu.jacktrain.energyapocalypse.game;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.core.Presets;
import eu.jacktrain.energyapocalypse.game.team.Team;

@SuppressWarnings("deprecation")
public class GameRespawn extends GameComponent implements Listener {
	private Game game;
	private EnergyApocalypse core;

	public GameRespawn(Game g) {

		game = g;
		core = game.getCore();
		core.getServer().getPluginManager().registerEvents(this, core);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void deathCheckEntity(EntityDamageByEntityEvent event) {
		if (event.getEntityType().equals(EntityType.PLAYER)) {
			Player ply = (Player) event.getEntity();
			Team toRemove = null;
			for (Team temp : game.getTeams()) {
				if (temp.isMember(ply)) {
					if (!(ply.getHealth() - event.getFinalDamage() > 0)) {
						event.setCancelled(true);

						if (temp.respawning()) {
							respawn(ply);
							ply.teleport(temp.getSpawn());
							if (event.getDamager() instanceof Player) {
								Player p = (Player) event.getDamager();
								game.bc(Presets.PREFIX + ply.getDisplayName()
										+ " was killed by "
										+ p.getDisplayName());
							} else {
								game.bc(Presets.PREFIX
										+ ply.getDisplayName()
										+ " was killed by "
										+ event.getDamager().getType()
												.getName());
							}
						} else {
							if (event.getDamager() instanceof Player) {
								Player p = (Player) event.getDamager();
								game.bc(Presets.PREFIX + ply.getDisplayName()
										+ " was killed by "
										+ p.getDisplayName()
										+ ", and dropped out");
								toRemove=temp;
							} else {
								game.bc(Presets.PREFIX
										+ ply.getDisplayName()
										+ " was killed by "
										+ event.getDamager().getType()
												.getName()
										+ ", and dropped out");
								toRemove=temp;
							}
						}

					}
				}
			}
			if (toRemove != null) {
				toRemove.removeMember(ply);
				game.getSpectators().addMember(ply);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void deathCheck(EntityDamageEvent event) {
		check(event);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void deathCheckBlock(EntityDamageByBlockEvent event) {
		check(event);
	}

	private void check(EntityDamageEvent event) {
		if (event.getEntityType().equals(EntityType.PLAYER)) {
			Team toRemove = null;
			Player ply = (Player) event.getEntity();
			for (Team temp : game.getTeams()) {
				if (temp.isMember(ply)) {
					if (!(ply.getHealth() - event.getFinalDamage() > 0)) {
						event.setCancelled(true);

						if (temp.respawning()) {
							respawn(ply);
							ply.teleport(temp.getSpawn());
							game.bc(Presets.PREFIX + ply.getDisplayName()
									+ " died");
						} else {
							game.bc(Presets.PREFIX + ply.getDisplayName()
									+ " dropped out");
							toRemove = temp;
							;
						}

					}
				}
			}

			if (toRemove != null) {
				toRemove.removeMember(ply);
				game.getSpectators().addMember(ply);
			}
		}
	}

	private void respawn(Player ply) {
		ply.setVelocity(new Vector(0,0,0));
		ply.setFallDistance(0F);
		ply.getInventory().clear();
		ply.getInventory().setBoots(null);
		ply.getInventory().setLeggings(null);
		ply.getInventory().setChestplate(null);
		ply.getInventory().setHelmet(null);
		ply.setFireTicks(0);
		ply.setFoodLevel(20);
		ply.setHealth(ply.getMaxHealth());
	}

	public void delete() {
		EntityDamageEvent.getHandlerList().unregister(this);
		EntityDamageByEntityEvent.getHandlerList().unregister(this);
		EntityDamageByBlockEvent.getHandlerList().unregister(this);
	}
}
