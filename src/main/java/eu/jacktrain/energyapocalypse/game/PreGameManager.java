package eu.jacktrain.energyapocalypse.game;

import java.util.HashSet;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.core.Presets;
import eu.jacktrain.energyapocalypse.game.team.Team;

public class PreGameManager implements Listener {

	private Game game;
	private HashSet<Player> player;
	private HashSet<UUID> disconected;

	public PreGameManager(Game ngame, EnergyApocalypse core) {
		player = new HashSet<Player>();
		game = ngame;
		core.getServer().getPluginManager().registerEvents(this, core);
		disconected = new HashSet<UUID>();
	}

	public void join(Player ply) {
		player.add(ply);
		ply.sendMessage(Presets.PREFIX + "You joined the queue for "
				+ game.getName());
	}

	public void start() {
		HashSet<Team> t = game.getTeams();
		Object[] teams = t.toArray();
		Object[] join = player.toArray();
		int nextTeam = 0;
		for (int i = 0; i < join.length; i++) {
			if (((Player) join[i]).isOnline()&&(!game.getCore().isInGame((Player)join[i]))) {
				((Team) teams[nextTeam]).addMember((Player) join[i]);
				nextTeam++;
			}
			if (!(nextTeam < teams.length)) {
				nextTeam = 0;
			}
		}
		player.clear();
	}

	public void leave(Player ply) {
		player.remove(ply);
		ply.sendMessage(Presets.PREFIX + "You left the queue for "
				+ game.getName());
	}

	public boolean isMember(Player ply) {
		return player.contains(ply);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void quitCheck(PlayerQuitEvent event) {
		Player ply = event.getPlayer();
		if (player.contains(ply)) {
			player.remove(ply);
			disconected.add(ply.getUniqueId());
		}

	}

	@EventHandler(priority = EventPriority.HIGH)
	public void joinCheck(PlayerJoinEvent event) {
		Player ply = event.getPlayer();
		if (disconected.contains(ply.getUniqueId())) {
			join(ply);
			disconected.remove(ply.getUniqueId());
		}
	}
}
