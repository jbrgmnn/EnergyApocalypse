package eu.jacktrain.energyapocalypse.game;

import java.util.List;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.world.ChunkUnloadEvent;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;

public class GameWorld extends GameComponent implements Listener {

	private Game game;
	private EnergyApocalypse core;
	private GameBounds bounds;

	public GameWorld(Game g) {
		game = g;
		core = game.getCore();
		core.getServer().getPluginManager().registerEvents(this, core);
		bounds = game.getBounds();
	}

	public void start() {
		bounds.getWorld().setAutoSave(false);
		bounds.getWorld().setPVP(true);
		core.getServer().getScheduler()
				.scheduleSyncDelayedTask(core, new Runnable() {
					public void run() {
						List<LivingEntity> entities = bounds.getWorld()
								.getLivingEntities();
						for (LivingEntity e : entities) {
							if (!(e instanceof Player)) {
								if (!bounds.isOutside(e.getLocation())) {
									e.setHealth(0);
								}
							}
						}
					}
				}, 10L);

	}

	@EventHandler(priority = EventPriority.HIGH)
	public void invCheck(CreatureSpawnEvent event) {
		if (event.getSpawnReason().equals(SpawnReason.NATURAL)
				&& (!game.getBounds()
						.isOutside(event.getEntity().getLocation()))) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void invCheck(ChunkUnloadEvent event) {
		if(!bounds.isOutside(event.getChunk())){
			event.setCancelled(true);
		}
	}
	public void delete() {
		CreatureSpawnEvent.getHandlerList().unregister(this);
		ChunkUnloadEvent.getHandlerList().unregister(this);
	}
}
