package eu.jacktrain.energyapocalypse.game;

import java.util.HashSet;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.game.team.Team;

public class GameBuild extends GameComponent implements Listener {
	private Game game;
	private EnergyApocalypse core;
	private static HashSet<Material> allowedMaterials = new HashSet<Material>();

	public GameBuild(Game g) {
		game = g;
		core = game.getCore();

		allowedMaterials.add(Material.RED_SANDSTONE);
		allowedMaterials.add(Material.WOOD);
		allowedMaterials.add(Material.GLASS);
		allowedMaterials.add(Material.CHEST);
		allowedMaterials.add(Material.NETHER_BRICK);

		core.getServer().getPluginManager().registerEvents(this, core);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void buildCheck(BlockBreakEvent event) {
		for (Team temp : game.getTeams()) {
			if (temp.isMember(event.getPlayer())) {
				if (!allowedMaterials.contains(event.getBlock().getType())) {
					event.setCancelled(true);
					break;
				}
			}
		}
	}

	public void delete() {
		BlockBreakEvent.getHandlerList().unregister(this);
	}
}
