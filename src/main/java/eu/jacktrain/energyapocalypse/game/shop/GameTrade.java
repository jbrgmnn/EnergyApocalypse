package eu.jacktrain.energyapocalypse.game.shop;

import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import eu.jacktrain.energyapocalypse.core.EnergyApocalypse;
import eu.jacktrain.energyapocalypse.game.Game;
import eu.jacktrain.energyapocalypse.game.GameComponent;

public class GameTrade extends GameComponent implements InventoryHolder,
		Listener {

	private Inventory shop;
	private HashSet<Trade> trades;
	private Game game;
	private EnergyApocalypse core;

	public GameTrade(Game ngame) {

		game = ngame;
		core = game.getCore();

		trades = new HashSet<Trade>();
		shop = Bukkit.createInventory(this, 54, "Equipment");
		shop.getViewers();

		Trade[] tradesarray = DefaultShopTrades.getDefaulttrades();
		ItemStack[] items = new ItemStack[tradesarray.length];
		for (int i = 0; i < items.length; i++) {
			if (tradesarray[i] != null) {
				trades.add(tradesarray[i]);
				items[i] = tradesarray[i].getDisplayItemStack();
			}
		}
		shop.setContents(items);
		core.getServer().getPluginManager().registerEvents(this, core);
	}

	public Inventory getInventory() {
		return shop;
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void actionCheck(InventoryClickEvent event) {
			if ((event.getInventory().getTitle().equals("Equipment"))&&game.isInGame((Player) event.getWhoClicked())) {
					trade((Player) event.getWhoClicked(),
							event.getCurrentItem());
				
				event.setCancelled(true);
			}

	}

	@EventHandler(priority = EventPriority.HIGH)
	public void invCheck(InventoryOpenEvent event) {
		if (event.getPlayer() instanceof Player) {
			if (game.isInGame((Player) event.getPlayer())
					&& (event.getInventory().getType()
							.equals(InventoryType.MERCHANT))) {
				event.setCancelled(true);
			}
		}

	}

	@EventHandler(priority = EventPriority.HIGH)
	public void invCheck(final PlayerInteractEntityEvent event) {
		if (game.isInGame((Player) event.getPlayer())
				&& (event.getRightClicked().getType()
						.equals(EntityType.VILLAGER))) {

			core.getServer().getScheduler()
					.scheduleSyncDelayedTask(core, new Runnable() {
						public void run() {
							event.getPlayer().openInventory(shop);
						}
					}, 1L);

		}

	}

	public void openShop(Player ply) {
		ply.openInventory(shop);
	}

	private void trade(Player whoClicked, ItemStack currentItem) {
		for (Trade t : trades) {
			if (t.getDisplayMaterial().equals(currentItem.getType())) {
				t.trade(whoClicked,game );
				break;
			}
		}

	}
	public void delete(){
		PlayerInteractEntityEvent.getHandlerList().unregister(this);
		InventoryOpenEvent.getHandlerList().unregister(this);
		InventoryOpenEvent.getHandlerList().unregister(this);
	}

}
