package eu.jacktrain.energyapocalypse.game.shop;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public abstract class DefaultShopTrades {

	private static final String n1 = "Clay Brick";
	private static final String n2 = "Iron Igniot";
	private static final String n3 = "Gold Igniot";

	public static Trade[] getDefaulttrades() {
		Trade[] temp = new Trade[54];
		// Blocks
		temp[0] = buildTrade(price1(1),Material.RED_SANDSTONE,2,"Basic build block");
		temp[1] = buildTrade(price1(5),Material.WOOD,2,"Advanced build Block");
		temp[3] = buildTrade(price2(1),Material.GLASS,1,"Advanced build Block");
		temp[2] = buildTrade(price1(5),Material.NETHER_BRICK,2,"Advanced build Block");
		// gadgets
		temp[9] = buildTrade(price2(1),Material.CHEST,1,"Storage");
		// weapons
		temp[18] = buildTrade(price1(5),Material.WOOD_SWORD,1,"Fight!");
		temp[19] = buildTrade(price2(4),Material.STONE_SWORD,1,"Fight harder!");
		temp[20] = buildTrade(price3(4),Material.IRON_SWORD,1,"Be the King");
		temp[21] = buildTrade(price2(4),Material.STONE_PICKAXE,1,"Build it");
		// armor
		temp[27] = buildTrade(price1(2),Material.LEATHER_HELMET,1,"Armor Lvl 1");
		temp[28] = buildTrade(price1(2),Material.LEATHER_CHESTPLATE,1,"Armor Lvl 1");
		temp[29] = buildTrade(price1(2),Material.LEATHER_LEGGINGS,1,"Armor Lvl 1");
		temp[30] = buildTrade(price1(2),Material.LEATHER_BOOTS,1,"Armor Lvl 1");
		temp[31] = buildTrade(price2(2),Material.CHAINMAIL_CHESTPLATE,1,"Armor Lvl 2");
		temp[32] = buildTrade(price2(2),Material.CHAINMAIL_LEGGINGS,1,"Armor Lvl 2");
		// food & suplies
		temp[45] = buildTrade(price1(1),Material.APPLE,1,"Eat Me");
		temp[46] = buildTrade(price2(1),Material.CAKE,1,"The Cake is a lie?");
		return temp;
	}

	public static Trade buildTrade(ItemStack price,Material m, int amount, String info){
		String pi = "";
		if(price.getType().equals(Material.CLAY_BRICK)){
			pi=n1;
		}
		else if(price.getType().equals(Material.IRON_INGOT)){
			pi=n2;
		}
		else if(price.getType().equals(Material.GOLD_INGOT)){
			pi=n3;
		}
		else{
			pi="free";
		}
		if(price.getAmount()>1){
			pi= price.getAmount()+" "+pi+"s";
		}
		else{
			pi= price.getAmount()+" "+pi;
		}
		ItemStack reward;
		if(info==null){
			reward = build(m,amount,pi);
		}
		else{
			reward = build(m,amount,pi,info);
		}
		return new Trade(price,reward);
	}
	
	
	public static ItemStack build(Material m, int amount,
			String price) {
		ItemStack i = new ItemStack(m, amount);
		ItemMeta im = i.getItemMeta();
		ArrayList<String> l = new ArrayList<String>();
		l.add("Price: " + price);
		im.setLore(l);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack build(Material m, int amount,
			String price, String info) {
		ItemStack i = new ItemStack(m, amount);
		ItemMeta im = i.getItemMeta();
		ArrayList<String> l = new ArrayList<String>();
		l.add("Price: " + price);
		l.add(info);
		im.setLore(l);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack price1(int amount) {
		return new ItemStack(Material.CLAY_BRICK, amount);
	}

	public static ItemStack price2(int amount) {
		return new ItemStack(Material.IRON_INGOT, amount);
	}

	public static ItemStack price3(int amount) {
		return new ItemStack(Material.GOLD_INGOT, amount);
	}
}
