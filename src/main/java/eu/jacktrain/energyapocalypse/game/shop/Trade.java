package eu.jacktrain.energyapocalypse.game.shop;

import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import eu.jacktrain.energyapocalypse.core.Presets;
import eu.jacktrain.energyapocalypse.game.Game;
import eu.jacktrain.energyapocalypse.game.team.Team;

public class Trade {

	private ItemStack price;
	private ItemStack reward;
	private static HashMap<ChatColor,Color> colors = new HashMap<ChatColor,Color>();

	public Trade(ItemStack nprice, ItemStack nreward) {
		colors.put(ChatColor.AQUA, Color.AQUA);
		colors.put(ChatColor.BLACK, Color.BLACK);
		colors.put(ChatColor.BLUE, Color.BLUE);
		colors.put(ChatColor.DARK_AQUA, Color.TEAL);
		colors.put(ChatColor.DARK_BLUE, Color.NAVY);
		colors.put(ChatColor.DARK_GRAY, Color.GRAY);
		colors.put(ChatColor.DARK_GREEN, Color.GREEN);
		colors.put(ChatColor.DARK_PURPLE, Color.PURPLE);
		colors.put(ChatColor.DARK_RED, Color.MAROON);
		colors.put(ChatColor.GOLD,Color.ORANGE);
		colors.put(ChatColor.GRAY, Color.SILVER);
		colors.put(ChatColor.GREEN, Color.LIME);
		colors.put(ChatColor.LIGHT_PURPLE, Color.FUCHSIA);
		colors.put(ChatColor.RED, Color.RED);
		colors.put(ChatColor.WHITE, Color.WHITE);
		colors.put(ChatColor.YELLOW, Color.YELLOW);
		price = nprice;
		reward = nreward;
	}

	public Material getDisplayMaterial() {
		return reward.getType();
	}

	public void trade(Player whoClicked, Game g) {
		Inventory inv = whoClicked.getInventory();
		if (inv.contains(price.getType(), price.getAmount())) {
			int count = 0;
			for (ItemStack i : whoClicked.getInventory()) {
				if (i == null) {
					count++;
				}
			}
			if (count > 0) {
				for (int i = 0; i < whoClicked.getInventory().getSize(); i++) {
					ItemStack itm = whoClicked.getInventory().getItem(i);
					if (itm != null && itm.getType().equals(price.getType())) {
						int amt = itm.getAmount() - price.getAmount();
						itm.setAmount(amt);
						whoClicked.getInventory().setItem(i,
								amt > 0 ? itm : null);
						break;
					}
				}
				ItemStack cr = reward.clone();
				if (reward.getType().equals(Material.LEATHER_HELMET)
						|| reward.getType().equals(Material.LEATHER_CHESTPLATE)
						|| reward.getType().equals(Material.LEATHER_LEGGINGS)
						|| reward.getType().equals(Material.LEATHER_BOOTS)) {
					LeatherArmorMeta im = (LeatherArmorMeta) cr.getItemMeta();
					im.setColor(getColor(whoClicked, g));
					cr.setItemMeta(im);
				}
				inv.addItem(cr);
			} else {
				whoClicked.sendMessage(Presets.PREFIX
						+ "There is not enough space in your inventory");
			}
		} else {
			whoClicked.sendMessage(Presets.PREFIX
					+ "You don't have enough items to pay");
		}

	}

	private Color getColor(Player whoClicked, Game g) {
		HashSet<Team> teams = g.getTeams();
		for (Team t : teams) {
			if (t.isMember(whoClicked)) {
				return colors.get(t.getC());
			}
		}
		return null;
	}

	public ItemStack getDisplayItemStack() {
		return reward;
	}

}
