package eu.jacktrain.energyapocalypse.core;

import java.util.HashSet;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityInteractEvent;

import eu.jacktrain.energyapocalypse.game.Game;
import eu.jacktrain.energyapocalypse.game.GameComponent;
import eu.jacktrain.energyapocalypse.game.GameVillager;
import eu.jacktrain.energyapocalypse.game.item.GameResources;
import eu.jacktrain.energyapocalypse.game.item.ItemDropper;
import eu.jacktrain.energyapocalypse.game.team.EnergySource;
import eu.jacktrain.energyapocalypse.game.team.Team;

public class Setup implements Listener {

	private Player ply;

	private int status;
	private GameCreater gc;
	private Game g;

	private HashSet<Team> tf;
	private String[] teams;
	private ChatColor[] teamcolor;
	private int current;
	private Location l;

	public Setup(String[] args, Player nply, GameCreater ngc) {
		ngc.getCore().getServer().getPluginManager()
		.registerEvents(this, ngc.getCore());
		gc = ngc;
		status = 0;
		ply = nply;
		if (args.length > 4) {
			ply.sendMessage(Presets.PREFIX + "Creating game: " + args[0]);
			String name = args[0];
			try{
			int x1 = new Integer(args[2]);
			int z1 = new Integer(args[3]);
			int x2 = new Integer(args[4]);
			int z2 = new Integer(args[5]);
			ply.sendMessage(Presets.PREFIX + "Setting bounds to: " + x1 + "|"
					+ z1 + "|" + x2 + "|" + z2);
			World w = ply.getLocation().getWorld();
			ply.sendMessage(Presets.PREFIX
					+ "Using your current world as game world");
			g = new Game(name, x1, z1, x2, z2, w, gc.getCore());

			ply.sendMessage(Presets.PREFIX
					+ "Starting scan for dropper blocks and Villager spawn blocks");
			int bounds[] = g.getBounds().getBounds();
			HashSet<ItemDropper> dropper = new HashSet<ItemDropper>();
			HashSet<Location> villager = new HashSet<Location>();
			for (int i = bounds[0]; i < bounds[2]; i++) {
				for (int n = bounds[1]; n < bounds[3]; n++) {
					for (int h = 0; h < 256; h++) {
						Location l = new Location(w, i, h, n);
						Block b = w.getBlockAt(l);
						if (b.getType().equals(Material.GOLD_BLOCK)) {
							dropper.add(new ItemDropper(l.add(0, 1, 0),
									Material.GOLD_INGOT));
						} else if (b.getType().equals(Material.IRON_BLOCK)) {
							dropper.add(new ItemDropper(l.add(0, 1, 0),
									Material.IRON_INGOT));
						} else if (b.getType().equals(Material.HARD_CLAY)) {
							dropper.add(new ItemDropper(l.add(0, 1, 0),
									Material.CLAY_BRICK));
						} else if (b.getType().equals(Material.LAPIS_BLOCK)) {
							villager.add(l.add(0, 1, 0));
						}
					}
				}
			}
			ply.sendMessage(Presets.PREFIX + "Added " + dropper.size()
					+ " Item dropper");
			ply.sendMessage(Presets.PREFIX + "Added " + villager.size()
					+ " Villager spawn blocks");
			HashSet<GameComponent> components = new HashSet<GameComponent>();
			components.add(new GameResources(dropper, gc.getCore()));
			components.add(new GameVillager(g, villager));
			g.setComponents(components);
			ply.sendMessage(Presets.PREFIX + "Starting teams creation");
			ply.sendMessage(Presets.PREFIX
					+ "Type: /a "
					+ g.getName()
					+ " teams <Team1Name> <Team1ChatColorChar> <Team2Name> <Team2ChatColorChar>...");
			}catch(Exception e){
				gc.remove(ply);
				ply.sendMessage(Presets.PREFIX+"Wrong data");
			}
			
		}
		else{
			gc.remove(ply);
			ply.sendMessage(Presets.PREFIX+"More data needed");
		}
		
	}

	public void cmd(String[] args, Player ply) {
		if (status == 0) {
			if (args[1].equals("teams")) {
				if (args.length > 5) {
					if (args.length % 2 == 0) {
						boolean s = true;
						teams = new String[args.length / 2 - 1];
						teamcolor = new ChatColor[args.length / 2 - 1];
						int n = 0;
						for (int i = 2; i < args.length; i = i + 2) {
							teams[n] = args[i];
							teamcolor[n] = ChatColor.getByChar(args[i + 1]);
							if (teamcolor[n] == null) {
								s = false;
							}
							n++;
						}
						if (s) {
							ply.sendMessage(Presets.PREFIX + teams.length
									+ "Teams registered");
							registerTeamPoints();
						} else {
							teams = null;
							teamcolor = null;
							ply.sendMessage(Presets.PREFIX
									+ "Failed to load the teams");
						}
					} else {
						ply.sendMessage(Presets.PREFIX
								+ "I feel no balance in your words");
					}
				} else {
					ply.sendMessage(Presets.PREFIX + "Give at least 2 teams");
				}
			} else {
				ply.sendMessage(Presets.PREFIX + "Invalid, try /a "
						+ args[0] + " teams");
			}
		}

	}

	private void registerTeamPoints() {
		status = 1;
		current = 0;
		tf = new HashSet<Team>();
		ply.sendMessage(Presets.PREFIX + "Place the spawn of " + teams[current]
				+ " next");
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void clickcheck(BlockPlaceEvent event) {
		if (status==1) {
			Player p =event.getPlayer();
			if (ply.equals(p)) {
				if (l == null) {
					l = event.getBlock().getLocation().add(0, 2, 0);
					ply.sendMessage(Presets.PREFIX + "Registered spawn for "
							+ teams[current]);
					ply.sendMessage(Presets.PREFIX
							+ "Place the EnergySource of " + teams[current]
							+ " next");
				} else {
					Team temp = new Team(l, teams[current], teamcolor[current],
							g);
					temp.setEnergySource(new EnergySource(event.getBlock()
							.getLocation(), temp, g));
					tf.add(temp);
					ply.sendMessage(Presets.PREFIX
							+ "Registered EnergySource for " + teams[current]);
					l=null;
					current++;
					if (current >= teams.length) {
						ply.sendMessage(Presets.PREFIX+"All points registered");
						finish();
					} else {
						ply.sendMessage(Presets.PREFIX + "Place the spawn of "
								+ teams[current] + " next");
					}
				}
			}

		}
	}

	private void finish() {
		status=2;
		EntityInteractEvent.getHandlerList().unregister(this);
		g.setTeams(tf);
		ply.sendMessage(Presets.PREFIX+"Saving game to disk");
		gc.getCore().getDatabaseManager().save(g);
		ply.sendMessage(Presets.PREFIX+"Game saved");
		gc.getCore().getDatabaseManager().load(g);
		ply.sendMessage(Presets.PREFIX+"Game available to play");
		gc.remove(ply);
	}

}
