package eu.jacktrain.energyapocalypse.core;

import java.util.HashMap;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameCreater {

	private HashMap<Player, Setup> setups;
	private EnergyApocalypse core;

	public GameCreater(EnergyApocalypse ncore) {
		core = ncore;
		setups = new HashMap<Player, Setup>();
	}

	public void load(String[] args, CommandSender sender) {
		if (sender.hasPermission(Presets.PREFIX_ADMIN + "create")) {
			if (sender instanceof Player) {
				Player ply = (Player) sender;
				if (setups.containsKey(ply)) {
					if (args.length > 1) {
						setups.get(ply).cmd(args, ply);
					} else {
						sender.sendMessage(Presets.PREFIX + "Give an operator");
					}
				} else {
					if (args.length > 1) {
						if (args[1].equals("create")) {
							setups.put(ply, new Setup(args, ply, this));
						} else {
							sender.sendMessage(Presets.PREFIX
									+ "Give a valid operator");
						}
					}

					else {
						sender.sendMessage(Presets.PREFIX + "Give an operator");

					}
				}
			} else {
				sender.sendMessage(Presets.NEED_PLAYER);
			}
		} else {
			sender.sendMessage(Presets.NO_PERM);
		}

	}

	public void remove(Player ply) {
		setups.remove(ply);
	}

	public EnergyApocalypse getCore() {
		return core;
	}
}
