package eu.jacktrain.energyapocalypse.core;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class ServerManager implements Listener {

	EnergyApocalypse core;

	public ServerManager(EnergyApocalypse ncore) {
		core = ncore;
		core.getServer().getPluginManager().registerEvents(this, core);
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGH)
	public void joinCheck(final PlayerJoinEvent event) {
		prefix(event.getPlayer());
		if ((!core.isInGame(event.getPlayer()))&&((!event.getPlayer().hasPermission(Presets.PERM_ADMIN+"join")))) {
			core.getServer().getScheduler()
					.scheduleAsyncDelayedTask(core, new Runnable() {

						public void run() {
							event.getPlayer().teleport(
									event.getPlayer().getLocation().getWorld()
											.getSpawnLocation());
						}
					}, 1L);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void breakheck(BlockBreakEvent event) {
		if (!core.isInGame(event.getPlayer())) {
			if(!event.getPlayer().hasPermission(Presets.PERM_ADMIN+"build")){
				event.setCancelled(true);
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void buildCheck(BlockPlaceEvent event) {
		if (!core.isInGame(event.getPlayer())) {
			if(!event.getPlayer().hasPermission(Presets.PERM_ADMIN+"build")){
				event.setCancelled(true);
			}
		}
	}
	
	
	@EventHandler(priority = EventPriority.HIGH)
	public void pvpCheckEntity(EntityDamageByEntityEvent event) {
		if (event.getDamager().getType().equals(EntityType.PLAYER)){
			if((!core.isInGame((Player) event.getDamager())&&(!((Player)event.getDamager()).hasPermission(Presets.PERM_ADMIN+"pvp")))){
				event.setCancelled(true);
			}

		}
	}

	public void prefix(Player ply){
		if(ply.hasPermission(Presets.PERM_ADMIN+"prefix")){
			ply.getPlayer().setDisplayName(Presets.PREFIX_ADMIN+ply.getName());
		}
	}
}
