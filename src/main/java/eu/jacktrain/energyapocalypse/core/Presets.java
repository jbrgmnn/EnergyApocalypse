package eu.jacktrain.energyapocalypse.core;

import org.bukkit.ChatColor;

public abstract class Presets {

	public final static String PREFIX = ChatColor.BLACK + "[" + ChatColor.RED
			+ "EA" + ChatColor.BLACK + "] " + ChatColor.WHITE;
	public final static String NO_PERM = PREFIX
			+ "You are not allowed to do this !";
	public final static String PERM_ADMIN = "energyApocalypse.admin.";
	public final static String PERM_PLAYER = "energyApocalypse.admin.";
	public static final String NEED_PLAYER = PREFIX
			+ "You must be a player to do this";
	public static String PREFIX_ADMIN = ChatColor.BLACK + "[" + ChatColor.DARK_RED
			+ "Team" + ChatColor.BLACK + "] " + ChatColor.WHITE;
}
