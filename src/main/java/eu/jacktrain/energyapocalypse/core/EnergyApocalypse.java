package eu.jacktrain.energyapocalypse.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.persistence.PersistenceException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import eu.jacktrain.energyapocalypse.database.BeanEnergySource;
import eu.jacktrain.energyapocalypse.database.BeanGame;
import eu.jacktrain.energyapocalypse.database.BeanItem;
import eu.jacktrain.energyapocalypse.database.BeanTeam;
import eu.jacktrain.energyapocalypse.database.BeanVillager;
import eu.jacktrain.energyapocalypse.database.DatabaseManager;
import eu.jacktrain.energyapocalypse.game.Game;

public class EnergyApocalypse extends JavaPlugin implements Listener {
	private HashSet<Game> games;
	private ServerManager servermanager;
	private GameCreater loader;
	private DatabaseManager database;
	private boolean usePlayPerm;

	public void onEnable() {
		
		defaultConfig();
		checkDatabase();
		
		loader = new GameCreater(this);
		games = new HashSet<Game>();
		database = new DatabaseManager(this);
		
		if (this.getConfig().getBoolean("ServerManager")) {
			servermanager = new ServerManager(this);
		}
		usePlayPerm = getConfig().getBoolean("usePlayPerm");

		database.loadAll();

	}

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (args.length > 0) {
			if (!cmd(args, sender)) {
				if (args[0].equalsIgnoreCase("list")) {
					if (sender.hasPermission(Presets.PERM_ADMIN + "list")) {
						sendList(sender);
					} else {
						sender.sendMessage(Presets.NO_PERM);
					}
				} else if (args.length > 1 && args[1].equalsIgnoreCase("delete")) {
					if (sender.hasPermission(Presets.PERM_ADMIN + "delete")) {
						for(Game g : games){
							if(g.getName().equals(args[0])){
								database.delete(g);
								sender.sendMessage(Presets.PREFIX + "Game " + args[0]
										+ " deleted");
								return true;
							}
						}
						sender.sendMessage(Presets.PREFIX+" No such game");
						return true;
						
					} else {
						sender.sendMessage(Presets.NO_PERM);
					}
				} else if (args[0].equalsIgnoreCase("servermanager")) {
					if (sender.hasPermission(Presets.PERM_ADMIN
							+ "servermanager")) {
						toggleServerManager(sender);
					} else {
						sender.sendMessage(Presets.NO_PERM);
					}
				} else if (args[0].equalsIgnoreCase("usePlayPerm")) {
					if (sender.hasPermission(Presets.PERM_ADMIN
							+ "usePlayPerm")) {
						toggleUsePlayPerm(sender);
					} else {
						sender.sendMessage(Presets.NO_PERM);
					}
				} else {
					loader.load(args, sender);
				}
			}
		} else {
			sender.sendMessage(Presets.PREFIX
					+ "EnergyApocalypse running (by @jacktrainEU http://jacktrain.eu )");
		}
		return true;

	}

	private void toggleUsePlayPerm(CommandSender sender) {
		if (this.getConfig().getBoolean("ServerManager")) {
			getConfig().set("usePlayPerm", false);
			sender.sendMessage(Presets.PREFIX
					+ "Play permissions disabled, please restart the server");
		} else {
			getConfig().set("usePlayPerm", true);
			sender.sendMessage(Presets.PREFIX
					+ "Play permissions enabled, please restart the server");
		}
		saveConfig();
		
	}

	private void sendList(CommandSender sender) {
		String msg = Presets.PREFIX + "All games: \n";
		for (Game g : games) {
			msg = msg + g.getName() + ":>" + g.getStatus()+" \n";
		}
		sender.sendMessage(msg);
	}

	private boolean cmd(String[] args, CommandSender sender) {
		for (Game g : games) {
			if (g.cmd(args, sender)) {
				return true;
			}
		}
		return false;
	}

	public void addGame(Game g) {
		games.add(g);
	}

	public boolean isInGame(Player ply) {
		for (Game g : games) {
			if (g.isInGame(ply)) {
				return true;
			}

		}
		return false;
	}

	private void defaultConfig() {
		getConfig().options().copyDefaults(true);
		getConfig().addDefault("ServerManager", true);
		getConfig().addDefault("useJoinPerm", false);
	}

	private void toggleServerManager(CommandSender sender) {
		if (this.getConfig().getBoolean("ServerManager")) {
			getConfig().set("ServerManager", false);
			sender.sendMessage(Presets.PREFIX
					+ "ServerManager disabled, please restart the server");
		} else {
			getConfig().set("ServerManager", true);
			sender.sendMessage(Presets.PREFIX
					+ "ServerManager enabled, please restart the server");
		}
		saveConfig();
	}

	private void checkDatabase() {
		if (!getDatabaseClasses().isEmpty()) {
			Class<?> temp = getDatabaseClasses().get(0);
			if (temp != null) {
				try {
					getDatabase().find(temp).findRowCount();
				} catch (PersistenceException ex) {
					getLogger().info(
							"Installing database due to first time usage");
					installDDL();
					getLogger().info("Installing database finished");
				}
			}
		}

	}

	public List<Class<?>> getDatabaseClasses() {
		ArrayList<Class<?>> temp = new ArrayList<Class<?>>();
		temp.add(BeanItem.class);
		temp.add(BeanGame.class);
		temp.add(BeanTeam.class);
		temp.add(BeanEnergySource.class);
		temp.add(BeanVillager.class);
		return temp;

	}

	public DatabaseManager getDatabaseManager() {
		return database;
	}

	public boolean isUsePlayPerm() {
		return usePlayPerm;
	}

	public void removeGame(Game g) {
		g.delete();
		games.remove(g);
		
	}
}
